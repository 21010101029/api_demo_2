import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:retrofit_flutter_2/add_user.dart';
import 'package:retrofit_flutter_2/model/FacultyModel.dart';
import 'package:retrofit_flutter_2/model/ObjectModel.dart';
import 'package:retrofit_flutter_2/restClient/rest_client.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        floatingActionButton:
            FloatingActionButton.extended(onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddUser(),
                  )).then(
                    (value) {
                  setState(() {
                    getFaculty();
                  });
                },
              );
            }, label: Text("ADD")),
        body: FutureBuilder(
          future: getFaculty(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "${snapshot.data!.resultList![index].name}",
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddUser(map: snapshot.data!.resultList![index]),
                                  )).then(
                                (value) {
                                  setState(() {
                                    getFaculty();
                                  });
                                },
                              );
                            },
                            child: Icon(
                              Icons.add,
                              color: Colors.orangeAccent,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              deleteFaculty(snapshot.data!.resultList![index].id
                                      .toString())
                                  .then(
                                (value) {
                                  setState(() {
                                    getFaculty();
                                  });
                                },
                              );
                            },
                            child: Icon(
                              Icons.delete,
                              color: Colors.redAccent,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: snapshot.data!.resultList!.length,
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }
}

Future<FacultyModel> getFaculty() async {
  final dio = Dio();
  final client = RestClient(dio);
  String data = await client.getTasks();
  FacultyModel data1 = FacultyModel.fromJson(jsonDecode(data));
  return data1;
}

Future<ObjectModel> getFacultyByID(id) async {
  final dio = Dio();
  final client = RestClient(dio);
  ObjectModel data = await client.getByID(id.toString());
  return data;
}

Future<void> deleteFaculty(id) async {
  final dio = Dio();
  final client = RestClient(dio);
  await client.deleteTask(id.toString());
}

Future<void> updateFaculty(id, ObjectModel map) async {
  final dio = Dio();
  final client = RestClient(dio);
  await client.updateTask(id.toString(), map);
}

Future<void> insert(ObjectModel map) async {
  final dio = Dio();
  final client = RestClient(dio);
  await client.createTask(map);
}
