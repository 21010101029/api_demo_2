import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:retrofit_flutter_2/model/ObjectModel.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: "https://630ce2de53a833c53437ab57.mockapi.io/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/faculties")
  Future<String> getTasks();

  @GET("/faculties")
  Future<ObjectModel> getByID(@Path("id") String id);

  @PUT("/faculties/{id}")
  Future<ObjectModel> updateTask(@Path("id") String id, @Body() ObjectModel task);

  @DELETE("/faculties/{id}")
  Future<void> deleteTask(@Path("id") String id);

  @POST("/faculties")
  Future<ObjectModel> createTask(@Body() ObjectModel task);
}
