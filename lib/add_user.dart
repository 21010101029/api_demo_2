import 'package:flutter/material.dart';
import 'package:retrofit_flutter_2/model/ObjectModel.dart';
import 'package:retrofit_flutter_2/my_home_page.dart';

class AddUser extends StatefulWidget {
  ObjectModel? map;

  AddUser({this.map});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  TextEditingController _createdAt = TextEditingController();
  TextEditingController _name = TextEditingController();
  TextEditingController _avatar = TextEditingController();
  TextEditingController _id = TextEditingController();

  @override
  void initState() {
    _name.text = widget.map != null ? widget.map!.name.toString() : '';
    _id.text = widget.map != null ? widget.map!.id.toString() : '';
    _createdAt.text =
        widget.map != null ? widget.map!.createdAt.toString() : '';
    _avatar.text = widget.map != null ? widget.map!.avatar.toString() : '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            TextFormField(
              controller: _createdAt,
              decoration: InputDecoration(labelText: "createdAt"),
            ),
            TextFormField(
              controller: _name,
              decoration: InputDecoration(labelText: "name"),
            ),
            TextFormField(
              controller: _avatar,
              decoration: InputDecoration(labelText: "avatar"),
            ),
            TextFormField(
              controller: _id,
              decoration: InputDecoration(labelText: "id"),
            ),
            TextButton(
                onPressed: () {
                  // Map<String ,dynamic> map={};
                  // map['createdAt']=_createdAt.text.toString();
                  // map['name']=_name.text.toString();
                  // map['avatar']=_avatar.text.toString();
                  // map['id']=_id.text.toString();
                  ObjectModel obj = ObjectModel(
                    id: _id.text.toString(),
                    name: _name.text.toString(),
                    avatar: _avatar.text.toString(),
                    createdAt: _createdAt.text.toString(),
                  );
                  if (widget.map != null) {
                    updateFaculty(widget.map!.id.toString(), obj)
                        .then((value) => Navigator.pop(context));
                  }else{
                    insert(obj).then((value) => Navigator.pop(context));
                  }
                },
                child: Icon(Icons.done_all_rounded)),
          ],
        ),
      ),
    );
  }
}
