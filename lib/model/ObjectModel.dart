import 'dart:convert';
/// createdAt : "2022-11-23T07:31:55.674Z"
/// name : "Dr. Fannie Klein"
/// avatar : "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1189.jpg"
/// id : "12"

ObjectModel objectModelFromJson(String str) => ObjectModel.fromJson(json.decode(str));
String objectModelToJson(ObjectModel data) => json.encode(data.toJson());
class ObjectModel {
  ObjectModel({
      String? createdAt, 
      String? name, 
      String? avatar, 
      String? id,}){
    _createdAt = createdAt;
    _name = name;
    _avatar = avatar;
    _id = id;
}

  ObjectModel.fromJson(dynamic json) {
    _createdAt = json['createdAt'];
    _name = json['name'];
    _avatar = json['avatar'];
    _id = json['id'];
  }
  String? _createdAt;
  String? _name;
  String? _avatar;
  String? _id;
ObjectModel copyWith({  String? createdAt,
  String? name,
  String? avatar,
  String? id,
}) => ObjectModel(  createdAt: createdAt ?? _createdAt,
  name: name ?? _name,
  avatar: avatar ?? _avatar,
  id: id ?? _id,
);
  String? get     createdAt => _createdAt;
  String? get name => _name;
  String? get avatar => _avatar;
  String? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['createdAt'] = _createdAt;
    map['name'] = _name;
    map['avatar'] = _avatar;
    map['id'] = _id;
    return map;
  }

}