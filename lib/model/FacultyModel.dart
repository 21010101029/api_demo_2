

import 'package:retrofit_flutter_2/model/ObjectModel.dart';

class FacultyModel{
  List<ObjectModel>? _resultList;
  LaptopModel({
    List<ObjectModel>? resultList,
  }){
    _resultList=resultList;
  }
  List<ObjectModel>? get resultList =>_resultList;
  FacultyModel.fromJson(var json){
    if(json!=null){
      _resultList=[];
      json.toList().forEach((v){
        _resultList?.add(ObjectModel.fromJson(v));
      });
    }
  }
}